# PAT

Our PUF analysis tool (PAT for brevity) can be used for estimating the quality factors of arbitrary PUF constructions. Both, uniqueness and reproducibility measures can be evaluated. Since the input is expected to consist of binary PUF responses, it can be used for all PUF constructions that generate binary data.
This manual has two aims: First, it explains PAT itself as well as its interfaces to the user. Second, examples are given to get an impression about its functioning.

See manual.pdf for more details and examples.

Best regards,
Sven Müelich, Sven Puchinger, and Veniamin Stukalov
