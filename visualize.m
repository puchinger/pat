function visualize(F_avg, sigma_D_pv_norm, sigma_D_noise_norm, HD_all_pairs, ...
                    HW, bit_aliasing,  reliab_norm, once_flipped_norm, ...
                    reliab_volt, reliab_temp, error_prob, one_prob, number_of_pufs, response_length)
                
%VISUALIZE according to Secure Embedded Systems (SES) Lab at Virginia Tech.
%   
%   visualize(F_avg, sigma_D_pv_norm, sigma_D_noise_norm, HD_all_pairs, ...
%                    HW, bit_aliasing,  reliab_norm, once_flipped_norm, ...
%                    reliab_volt, reliab_temp, error_prob, one_prob)
%
%   VISUALIZE function plots results of frequency dataset analysis and 
%   analysis of PUF response set.
%   It follows the pattern of the articles "A Large Scale Characterization of RO-PUF
%   by Secure Embedded Systems (SES) Lab at Virginia Tech, 2010" and
%   "A Soft Decision Helper Data Algorithm for SRAM PUFs by Roel Maes et al."
% 
%   In order to omit a certain graph, put 'NaN' at the corresponding 
%   parameter's position.

    x = 1 : response_length;
    
    if ~isnan(F_avg)
        figure; histogram(F_avg, 20)
        title('Distribution of  individual FPGAs avg. RO frequency.')
        xlabel('RO Frequencies') % x-axis label
        ylabel('Number of chips') % y-axis label
        % Second graph...
        figure; 
        p1 = plot(F_avg,'-o','MarkerIndices',[find(F_avg == max(F_avg)),find(F_avg == min(F_avg))],'MarkerFaceColor','red');
        title('Global average of RO frequency.')
        xlabel('FPGA number') % x-axis label
        ylabel('Frequency, MHz') % y-axis label
        xmin = x(F_avg == min(F_avg));
        ymin = F_avg(F_avg == min(F_avg));
        strmin = ['Min = ', num2str(ymin,'%.3f')];
        xmax = x(F_avg == max(F_avg));
        ymax = F_avg(F_avg == max(F_avg));
        strmax = ['Max = ', num2str(ymax,'%.3f')];
        text(xmin+1, ymin, strmin,'HorizontalAlignment','left');
        text(xmax, ymax+1, strmax,'HorizontalAlignment','center');
        hold on
        F = mean(F_avg);
        p2 = line([1 193],[F F],'Color','g');
        hold off
        legend([p1 p2],'F avg.(FPGA number)',strcat('Global avg. of F=', num2str((F),'%.1f'), 'MHz'),'Location','southeast');
    end
    
    if ~isnan(sigma_D_pv_norm)
        figure; histogram(sigma_D_pv_norm, 20)
        title('Distribution of static intra-die variation among all FPGAs.')
        xlabel('Static intra-die variation, %') % x-axis label
        ylabel('Number of FPGAs') % y-axis label
        ymin = sigma_D_pv_norm(sigma_D_pv_norm == min(sigma_D_pv_norm));
        strmin = ['Min = ', num2str(ymin(1),'%.2f%%')];
        ymax = sigma_D_pv_norm(sigma_D_pv_norm == max(sigma_D_pv_norm));
        strmax = ['Max = ', num2str(ymax(1),'%.2f%%')];
        stravg = ['Avg. static variation: ', num2str(mean(sigma_D_pv_norm),'%.2f%%')];
        annotation('textbox',[0.55 0.75 0.1 0.1],'String',{strmax,strmin,stravg},'FitBoxToText','on');
    end
    
    if ~isnan(sigma_D_noise_norm)
        figure; histogram(sigma_D_noise_norm,240)
        xlabel('Dynamic intra-die variation, %') % x-axis label
        ylabel('Number of FPGAs') % y-axis label
        title('Distribution of dynamic intra-die variation among all FPGAs.')
        stravg = ['Avg. dynamic variation: ', num2str(mean(sigma_D_noise_norm),'%.3f%%')];
        annotation('textbox',[0.5 0.65 0.1 0.1],'String',stravg,'FitBoxToText','on');
    end
    
    if ~isnan(HD_all_pairs)
        figure; histogram(HD_all_pairs, 20)
        title('Distribution of the inter-die Hamming distance.')
        xlabel('Inter-die Hamming distance, %') % x-axis label
        ylabel('Number of comparison pairs') % y-axis label
        ymin = min(HD_all_pairs);
        strmin = ['Min = ', num2str(ymin,'%.2f%%')];
        ymax = max(HD_all_pairs);
        strmax = ['Max = ', num2str(ymax,'%.2f%%')];
        stravg = ['Avg. HD = ', num2str(mean(HD_all_pairs),'%.2f%%')];
        annotation('textbox',[0.67 0.38 0.5 0.5],'String',{strmin,strmax,stravg},'FitBoxToText','on');
    end
    
    if ~isnan(HW)
        figure; 
        p1 = plot(HW);
        xlim([1 number_of_pufs]);
        title('Percentage Hamming weight of PUF responses.')
        xlabel('FPGA number') % x-axis label
        ylabel('HW, %') % y-axis label
        ymin = HW(HW == min(HW));
        strmin = strcat('Min = ', num2str(ymin(1),'%.2f%%'));
        ymax = HW(HW == max(HW));
        strmax = strcat('Max = ', num2str(ymax(1),'%.2f%%'));
        avg = mean(HW);
        stravg = strcat('Avg = ', num2str(avg,'%.2f%%'));
        hold on
        p2 = line([1 number_of_pufs],[ymin(1) ymin(1)],'Color','magenta');
        p3 = line([1 number_of_pufs],[ymax(1) ymax(1)],'Color','red');
        p4 = line([1 response_length],[avg avg],'Color','green');
        hold off
        legend([p1 p2 p3 p4],'ref. resp. HW',strmin,strmax, stravg, 'Location','northeast');
    end
    
    if ~isnan(bit_aliasing)
        avg = mean(bit_aliasing);
        figure; 
        p1 = plot(bit_aliasing);
        xlim([1 response_length]);
        title('Bit-aliasing as percentage Hamming weight.')
        xlabel('Bit position') % x-axis label
        ylabel('Bit value repitativity, %') % y-axis label
        ymin = bit_aliasing(bit_aliasing == min(bit_aliasing));
        strmin = strcat('Min = ', num2str(ymin(1),'%.2f%%'));
        ymax = bit_aliasing(bit_aliasing == max(bit_aliasing));
        strmax = strcat('Max = ', num2str(ymax(1),'%.2f%%'));
        stravg = strcat('Avg = ', num2str(avg,'%.2f%%'));
        hold on
        p2 = line([1 response_length],[ymin(1) ymin(1)],'Color','magenta');
        p3 = line([1 response_length],[ymax(1) ymax(1)],'Color','red');
        p4 = line([1 response_length],[avg avg],'Color','green');
        hold off
        legend([p1 p2 p3 p4],'bit-aliasing',strmin,strmax, stravg, 'Location','southeast');
    end
    
    if ~isnan(reliab_norm) 
        figure; histogram(reliab_norm, 20)
        title('Distribution of intra-die Hamming distance.')
        xlabel('Intra-die Hamming distance, %') % x-axis label
        ylabel('Number of FPGAs') % y-axis label
        ymin = reliab_norm(reliab_norm == min(reliab_norm));
        strmin = ['Min = ', num2str(ymin,'%.2f%%')];
        ymax = reliab_norm(reliab_norm == max(reliab_norm));
        strmax = ['Max = ', num2str(ymax,'%.2f%%')];
        stravg = ['Avg. intra-die HD: ', num2str(mean(reliab_norm),'%.2f%%')];
        annotation('textbox',[0.55 0.75 0.1 0.1],'String',{strmax,strmin,stravg},'FitBoxToText','on');
    end
    
    if ~isnan(once_flipped_norm)
        figure; histogram(once_flipped_norm, 20)
        title('Distinct unstable bits at normal condition.')
        xlabel('Distinct unstable bits') % x-axis label
        ylabel('Number of FPGAs') % y-axis label
        ymin = once_flipped_norm(once_flipped_norm == min(once_flipped_norm));
        strmin = ['Min = ', num2str(ymin(1),'%.2f%%')];
        ymax = once_flipped_norm(once_flipped_norm == max(once_flipped_norm));
        strmax = ['Max = ', num2str(ymax(1),'%.2f%%')];
        stravg = 'Avg.unst. bits/FPGA:';
        annotation('textbox',[0.65 0.75 0.1 0.1],'String',{strmax,strmin,stravg,...
            num2str(mean(once_flipped_norm),'%.2f%%')},'FitBoxToText','on');
    end
    
    if ~isnan(reliab_volt)
        figure;
        subplot(2,1,1)
        hold on
        p1 = plot(reliab_volt(1:5),'-o');
        p2 = plot(reliab_volt(6:10),'-o');
        p3 = plot(reliab_volt(11:15),'-o');
        p4 = plot(reliab_volt(16:20),'-o');
        p5 = plot(reliab_volt(21:25),'-o');
        xlim([1 5]);
        hold off
        legend([p1 p2 p3 p4 p5],'chip 1','chip 2','chip 3','chip 4','chip 5','Location','southeastoutside')
        title('Voltage variation.')
        xlabel('Voltage, V') % x-axis label
        ylabel('Intra-die HD, %') % y-axis label
        xticks(1:5)
        xticklabels({'0.96','1.08','1.20','1.32','1.44'})
    end
  
    if ~isnan(reliab_temp)
        subplot(2,1,2)
        hold on
        p6 = plot(reliab_temp(1:5),'-o');
        p7 = plot(reliab_temp(6:10),'-o');
        p8 = plot(reliab_temp(11:15),'-o');
        p9 = plot(reliab_temp(16:20),'-o');
        p10 = plot(reliab_temp(21:25),'-o');
        xlim([1 5]);
        hold off
        legend([p6 p7 p8 p9 p10],'chip 1','chip 2','chip 3','chip 4','chip 5','Location','southeastoutside')
        title('Temperature variation.')
        xlabel('Temp., C') % x-axis label
        ylabel('Intra-die HD, %') % y-axis label
        xticks(1:5)
        xticklabels({'25','35','45','55','65'})
    end
    
    if ~isnan(error_prob)
        figure;
        set(gca,'YScale','log');
        histogram(error_prob, 20);
        set(gca,'YScale','log');
        title('Response error probability.')
        xlabel('Probability of bit error, Pr_e') % x-axis label
        ylabel('Number of bits with certain Pr_e') % y-axis label
    end
    
    if ~isnan(one_prob)
        figure;
        set(gca,'YScale','log');
        histogram(one_prob, 20);
        set(gca,'YScale','log');
        title('One probability')
        xlabel('Probability to observe one') % x-axis label
        ylabel('Number of bits with certain probability') % y-axis label
    end
       
end

