clear all;
close all;
clc;

%% Analyze uniqueness. %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%result = main_analysis('Data/ForUniqueness/initial_test1.csv', 'inter', 5, 1);
%result = main_analysis('Data/ForUniqueness/initial_test2.csv', 'inter', 3, 1);

%% Analyze reproducibility of one PUF. %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%result = main_analysis('Data/ForReproducibility/*.csv', 'intra', 5, 10);