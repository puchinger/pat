function result = main_analysis(csv_file, mode, response_length, number_of_readouts) 

% Input:
%   1.) in case of inter tests: csv-file (choose file which you want to use for analysis)
%       csv-file: char of path of file.
%        in case of intra tests: directory followed by /*.csv in which csv-files of several PUFs are located.
%   2.) mode: char \in {'inter','intra'} to decide whether analyis
%       of inter-distance (1 initial bit sequence per device) or analysis
%       of intra-distance has to be applied.
%   3.) response_length: integer, response length user wants to have.
%   4.) number_of_readouts: number of readouts including initial readouts
%       in case of intra-distance analysis or 
%       1 by definition in case of inter-distance analysis.


    % Checker whether inputs have correct data type.
    if(ischar(csv_file)~=1)
        error('Error: Input csv_file has wrong data type.\n');
    end
    
    if(ischar(mode)~=1)
        error('Error: Input mode has wrong data type.\n');
    end
    
    if(isa(response_length, 'double')~=1)
        error('Error: Input response_length has wrong data type.\n');
    end

    % Select whether we are in "intra"- or "inter" mode.
    switch mode

    % Analysis of uniqueness. %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case 'inter'
        fprintf('Mode "inter" was received as input.\n');
        fid = fopen(csv_file, 'r');
        % In case file could not be opened, exit.
        if(fid==-1)
           error('Error: File "%s" could not be opened.\n', csv_file);  
        end
        response_ref = dlmread(csv_file);
        dimension = size(response_ref);
        number_of_pufs = dimension(1);
        number_of_bits = dimension(2);
        
        % check if enough response bits are provided by data set.
        if(response_length > number_of_bits)
            fprintf('Only responses of length %d (instead of %d) are generated due to availability.\n', number_of_bits, response_length);  
        else
            response_ref = response_ref(:,1:response_length);
        end
        
        % Hamming weights of reference responses for each chip in population.
        HW = mean(response_ref, 2) * 100; 
        
        % Bit aliasing of reference responses.
        bit_aliasing = mean(response_ref,1) * 100;
        
        % HD of all pairs.
        HD_all_pairs = zeros(1,nchoosek(number_of_pufs,2));

        i = 1;
        for u = 1:number_of_pufs
            for v = (u+1):number_of_pufs
                HD_all_pairs(i) = HD(response_ref(u,:), response_ref(v,:));
                i = i + 1;
            end
        end

        % plot graphs.
        visualize(NaN,NaN,NaN,HD_all_pairs, HW,bit_aliasing,NaN,NaN,NaN,NaN,NaN,NaN,number_of_pufs,response_length);
        

    % Analysis of reproducibility. %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case 'intra'
         fprintf('Mode "intra" was received as input.\n');
         files = dir(csv_file);
         %number_of_readouts = length(files)

         
         M = zeros(number_of_readouts, response_length, length(files));
         i = 1;

         for file = files'
            csv = load(file.name);    
            % create 3dimensional matrix.
            M(:,:,i) = csv;
            i = i+1;
         end
 
         
          % average intra-response hd of all chips.
          w = zeros(1,response_length+1);
          for j = 1:length(files)
              for v = 2:number_of_readouts
                d = sum(M(1,:,j) ~= M(v,:,j));
                w(d+1) = w(d+1) + 1;
              end
          end
          strmin = min(find(w~=0)) -1; % -1 Indexkorrektur
          strmax = max(find(w~=0)) -1; % -1 Indexkorrektur
          w = w./sum(w(:));
          stravg = sum((0:response_length).*w)/response_length*100;

          x_axis = 0:response_length;
          %x_axis = x_axis/length(x_axis);
          bar(x_axis,w)
          xlim([0 response_length])
          %xlim([0 1])
          xlabel('Intra-response Hamming distance')
          ylabel('Number of occurences')
          strmin = ['Min = ', num2str(strmin,'%d')];
          strmax = ['Max = ', num2str(strmax,'%d')];
          stravg = ['Avg = ', num2str(stravg,'%.2f%%')];
          annotation('textbox',[0.67 0.38 0.5 0.5],'String',{strmin,strmax,stravg},'FitBoxToText','on')

        

        
    % Invalid input. %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    otherwise
        error('Error: Mode "%s" which was received as input does not exist.\n',mode);
    end
        
result = 0. %success 

end