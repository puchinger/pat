function c = HD(a,b)
%HD percentage Hamming distance
%
%   c = HD(a,b)
%
%   Returns Hamming distance in percent beetween two vectors represented in a form of 
%   one dimentional array provided as input arguments for HD function. Arrays must be of 
%   double type and same lengths.

c = 0;

for i = 1:length(a)
        c = c + xor(a(i),b(i)); 
end

c = c * 100 / length(a);

end
